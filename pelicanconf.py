#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Timothy'
SITENAME = 'yhtom.it'
SITEURL = 'http://localhost:8000'

PATH = 'content'

TIMEZONE = 'Europe/Vienna'

DEFAULT_LANG = 'en'

THEME = 'pelican-cait'

USE_CUSTOM_MENU = True
CUSTOM_MENUITEMS = (('About', 'pages/about'),
                    ('Index', 'blog_index.html'))
PAGE_URL = 'pages/{slug}'
ARTICLE_URL = '{slug}'
INDEX_SAVE_AS = 'blog_index.html'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
